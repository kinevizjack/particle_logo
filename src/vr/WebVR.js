class WebVRHandler {
    constructor(renderer) {
        if (!renderer) {
            throw new Error("Miss renderer")
        }
        this.isWebXR = false;

        console.log(renderer)

        this.renderer = renderer;
        this.renderer.vr.enabled = false;
        // this.renderer.vr.enabled = false;
        this.button = document.createElement('div');
        this.button.textContent = 'VR NOT FOUND';
        this.button.style.display = 'none';
        // this.button.style.border = '1px solid #9b9b9b';
        // this.button.style.minWidth = '110px';
        // this.button.style.padding = '3px 8px';
        // this.button.style.fontWeight = '600px';
        this.button.style.cursor = "pointer";

        this._initVRDisplays();

        this.button.onclick = () => {
            this.enterVR(this.isPresenting());
        };
    }

    getDisplay() {
        if (!this.renderer) {
            throw Error("Miss renderer");
        }
        return this.renderer.vr.getDevice();
    }

    isPresenting() {
        return this.getDisplay() ? this.getDisplay().isPresenting : false;
    }

    enterVR(isEnter) {
        if (this.isWebXR) {
            return this.enterXR(isEnter);
        }

        return new Promise((resolve, reject) => {

            if (this.getDisplay()) {
                let display = this.getDisplay();
                if (isEnter && !display.isPresenting) {
                    this.renderer.vr.enabled = true;
                    this.renderer.gammaInput = true;
                    this.renderer.gammaOutput = true;
                    this.renderer.shadowMap.enabled = true;

                    console.log(this.renderer.domElement)
                    console.log(display.requestPresent)

                    display.requestPresent([{ source: this.renderer.domElement }]).then(() => {
                        resolve("done");
                    }).catch((err) => {
                        console.error(err)
                        reject(err);
                    })
                } else if (!isEnter && display.isPresenting) {
                    this.renderer.vr.enabled = false;
                    this.renderer.gammaInput = false;
                    this.renderer.gammaOutput = false;
                    this.renderer.shadowMap.enabled = false;
                    display.exitPresent().then(() => {
                        resolve("done");
                    }).catch((err) => {
                        console.error(err)
                        reject(err);
                    })
                }
            } else {

                console.warn("Can't found the display");
                reject(new Error("Can't found the display"));

            }

        })



    }

    enterXR(isEnter) {

        return new Promise((resolve, reject) => {


            if (!this.getDisplay()) {
                return reject(new Error("Can't found the display"));
            }
            let display = this.getDisplay();

            let currentSession = null;

            function onSessionStarted(session) {

                // if (options === undefined) {
                let options = {};
                // }
                // if (options.frameOfReferenceType === undefined) 
                options.frameOfReferenceType = 'stage';
                session.addEventListener('end', onSessionEnded);
                this.renderer.vr.setSession(session, options);
                currentSession = session;
                resolve("done")

            }

            function onSessionEnded(event) {
                currentSession.removeEventListener('end', onSessionEnded);
                this.renderer.vr.setSession(null);
                currentSession = null;
                resolve("done")
            }

            if (currentSession === null) {
                display.requestSession({ exclusive: true })
                    .then(onSessionStarted)
                    .catch((err) => {
                        console.error(err)
                        reject(err);
                    })
            } else {
                currentSession.end().catch((err) => {
                    console.error(err)
                    reject(err);
                })
            }
        })


    }


    getButton() {
        return this.button;
    }

    _updateVRText() {
        let display = this.getDisplay();
        if (!display) {
            this.button.textContent = 'VR NOT FOUND';
        } else {
            this.button.textContent = display.isPresenting ? 'EXIT VR' : 'ENTER VR';
        }

        //@travis , please un-comment the code if you want remove vr button text;
        this.button.textContent = "";

    }

    _updateDevice(device) {

        this.renderer.vr.setDevice(device);

        if (device) {
            this.button.style.display = '';
        } else {
            this.button.style.display = 'none';
        }

        this._updateVRText();
    }


    _initVRDisplays() {

        this.isWebXR = false;

        if ('xr' in navigator) {

            this.isWebXR = true;

            navigator.xr.requestDevice().then(function (device) {

                device.supportsSession({ exclusive: true }).then(function () {

                    this._updateDevice(device);

                }).catch(() => {
                    this._updateDevice(null)
                });

            }).catch(() => {
                this._updateDevice(null)
            });


        } else if ('getVRDisplays' in navigator) {

            navigator.getVRDisplays().then((displays) => {
                // If a display is available, use it to present the scene
                if (displays.length > 0) {
                    this._updateDevice(displays[0]);
                } else {
                    console.log("can't found the VR display");
                }
            });


            window.addEventListener('vrdisplayconnect', (event) => {
                this._updateDevice(event.display)
                console.log("vrdisplayconnect");
            }, false);

            window.addEventListener('vrdisplaydisconnect', (event) => {
                console.log("vrdisplaydisconnect");
                this._updateDevice(null)
            }, false);

            window.addEventListener('vrdisplaypresentchange', (event) => {
                console.log("vrdisplaypresentchange");
                this._updateDevice(event.display);
                this._updateVRText();
            });
            window.addEventListener('vrdisplaypointerrestricted', () => this._handlePointerRestricted(true), false);
            window.addEventListener('vrdisplaypointerunrestricted', () => this._handlePointerRestricted(false), false);

            // window.addEventListener('resize', (event) => this._handleWindowResize(), false);

            window.addEventListener('vrdisplayactivate', (event) => {
                this._updateDevice(event.display);
                this.enterVR(true);

            }, false);

        }
    }

    // _handleWindowResize() {
    //     let display = this.getDisplay();
    //     if (display && this.renderer) {
    //         // window.addEventListener( 'resize', onWindowResize, false );
    //         // this.renderer.setPixelRatio( window.devicePixelRatio );
    // 		this.renderer.setSize( window.innerWidth, window.innerHeight );
    //      }
    // }

    _handlePointerRestricted(isRestricted) {
        let pointerLockElement = this.renderer.domElement;
        if (isRestricted && pointerLockElement && typeof (pointerLockElement.requestPointerLock) === 'function') {
            pointerLockElement.requestPointerLock();
        } else {
            let currentPointerLockElement = document.pointerLockElement;
            if (currentPointerLockElement && currentPointerLockElement === pointerLockElement && typeof (document.exitPointerLock) === 'function') {
                document.exitPointerLock();
            }
        }
    }

}



const WebVR = function (renderer) {

    console.log(renderer)

    const _VRHandler = new WebVRHandler(renderer);

    this.getButton = _VRHandler.getButton.bind(_VRHandler);
    this.getDisplay = _VRHandler.getDisplay.bind(_VRHandler);

    this.isAvailable = function () {
        return 'getVRDisplays' in navigator || 'xr' in navigator
    }

    this.isPresenting = _VRHandler.isPresenting.bind(_VRHandler);

    this.enterVR = _VRHandler.enterVR.bind(_VRHandler);
}

WebVR.isAvailable = 'getVRDisplays' in navigator || 'xr' in navigator;
WebVR.isPresenting = function (renderer) {
    if (!renderer) {
        console.error("Miss parameter renderer");
        return false;
    }

    return renderer.vr && renderer.vr.getDevice() && renderer.vr.getDevice().isPresenting
}

export default WebVR

export { WebVR }