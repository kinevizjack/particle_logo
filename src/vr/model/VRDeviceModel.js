'use strict';

// import * as THREE from 'three'
// require("./OBJLoader.js");
// require("./MTLLoader.js");

let THREE = window.THREE

let S_ShareVRDeviceModel = null;
class VRDeviceModel {

	static getShareInstance() {
		if (!S_ShareVRDeviceModel) {
			S_ShareVRDeviceModel = new VRDeviceModel();
		}
		return S_ShareVRDeviceModel;
	}

	static getVRModel(modelName, type) {

		return new Promise((resolve, reject) => {
			if (modelName == 'vive') {
				this.getShareInstance().getViveControllerModel(function (mesh) {
					resolve(mesh);
				});
			} else {
				this.getShareInstance().getRiftControllerModel(type, function (mesh) {
					resolve(mesh);
				});
			}
		})
	}

	static getViveControllerModel(callback) {
		this.getShareInstance().getViveControllerModel(callback);
	}

	static getRiftControllerModel(type, callback, id) {
		this.getShareInstance().getRiftControllerModel(type, callback, id);
	}

	constructor() {
		this._caches = {
			vive: null,
			rift: {
				left: null,
				right: null
			}
		}
	}

	getViveControllerModel(callback) {


		if (this._caches.vive) {
			return callback(this._caches.vive.clone());
		} else {
			let loader = new THREE.OBJLoader();
			loader.setPath('/public/models/viveController/');
			loader.load('vr_controller_vive_1_5.obj', (object) => {

				let textureloader = new THREE.TextureLoader();
				textureloader.setPath('/public/models/viveController/');

				object.children[0].material.map = textureloader.load('onepointfive_texture.png');
				object.children[0].material.specularMap = textureloader.load('onepointfive_spec.png');

				object.children[0].castShadow = true;
				object.name = 'vive';

				this._caches.vive = object;

				return callback(this._caches.vive.clone());
			});
		}
	}

	getRiftControllerModel(type, callback, id) {
		let temp_m = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, wireframe: false, transparent: true, opacity: 0.8 })
		switch (type) {
			case "left": {

				if (this._caches.rift[type]) {
					return callback(this._caches.rift[type]);
				}

				let mtlLoader = new THREE.MTLLoader();
				mtlLoader.setPath('/public/models/riftController/');
				mtlLoader.load('oculus-touch-controller-left.mtl', (materials) => {
					materials.preload();

					let objLoader = new THREE.OBJLoader();

					// objLoader.setMaterials(materials);
					objLoader.setPath('/public/models/riftController/');

					objLoader.load('oculus-touch-controller-left.obj', (object) => {
						object.children[0].castShadow = false;

						object.traverse(c => {
							if (c instanceof THREE.Mesh) c.material = temp_m
						})
						object.name = 'rift-' + type;

						object.rotation.x = (Math.PI / 4);
						object.position.y += 0.075;
						object.position.x += (type === 'left') ? -0.0125 : 0.0125;

						this._caches.rift[type] = object;
						callback(this._caches.rift[type].clone());
					});
				});
				break;
			}
			case "right":
				{

					if (this._caches.rift[type]) {
						return callback(this._caches.rift[type]);
					}

					let mtlLoader = new THREE.MTLLoader();
					mtlLoader.setPath('/public/models/riftController/');
					mtlLoader.load('oculus-touch-controller-right.mtl', (materials) => {
						materials.preload();

						let objLoader = new THREE.OBJLoader();

						// objLoader.setMaterials(materials);
						objLoader.setPath('/public/models/riftController/');

						objLoader.load('oculus-touch-controller-right.obj', (object) => {

							object.children[0].castShadow = false;
							object.traverse(c => {
								if (c instanceof THREE.Mesh) {
									c.material = temp_m
								}
							})
							object.name = 'rift-' + type;

							object.rotation.x = (Math.PI / 4);
							object.position.y += 0.075;
							object.position.x += (type === 'left') ? -0.0125 : 0.0125;

							this._caches.rift[type] = object;
							callback(this._caches.rift[type].clone());
						});
					});
					break;
				}
		}
	}
}

export default VRDeviceModel