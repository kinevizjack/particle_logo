// import * as THREE from 'three'
import _ from 'lodash'
let THREE = window.THREE

THREE.VRController = function (id, debug) {

	THREE.Object3D.call(this);

	let scope = this;
	let gamepad;

	let axes = [0, 0];
	let buttonsState = {
		0: false, //'thumbpad',
		1: false, //'trigger',
		2: false, // 'grips',
		3: false, // "menu"
	};

	let buttonNames = {
		0: 'thumbpad',
		1: 'trigger',
		2: 'grips',
		3: 'menu'
	}

	let lastPosePosition = new THREE.Vector3();


	function findGamepad(id) {

		// Iterate across gamepads as Vive Controllers may not be
		// in position 0 and 1.

		let gamepads = navigator.getGamepads && navigator.getGamepads();

		gamepads = _.filter(gamepads,d => d !== null);

		for (let i = 0, j = 0; i < gamepads.length; i++) {

			let gamepad = gamepads[i];

			// if (gamepad && gamepad.displayId > 0) { //must VR gamepad

			if (gamepad && (gamepad.id === 'OpenVR Gamepad' || gamepad.id.startsWith('Oculus Touch') || gamepad.id.startsWith('Spatial Controller'))) {

				if (gamepad.id === id || (gamepad.index === id && gamepad.index !== undefined) || (gamepad.index === undefined && id === j)) {
					return gamepad;
				}

				j++;

			}

		}

	}
	this.standingMatrix = new THREE.Matrix4();
	this.matrixAutoUpdate = false;

	this.getGamepad = function () {

		return gamepad;

	};

	this.getPosePosition = function () {
		return lastPosePosition.clone();
	}

	this.getButtonState = function (buttonName) {

		if (buttonsState[buttonName] !== undefined) {
			return buttonsState[buttonName] || false;
		} else {
			return buttonsState[Object.keys(buttonNames).findIndex(function (index) {
				return buttonNames[index] === buttonName;
			})] || false;
		}

	};

	this.update = function () {

		gamepad = findGamepad(id);

		if (gamepad !== undefined && gamepad.pose !== undefined) {

			if (gamepad.pose === null) return; // No user action yet

			//  Position and orientation.

			let pose = gamepad.pose;

			if (pose.position !== null) {
				scope.position.fromArray(pose.position);
				lastPosePosition.fromArray(pose.position);
			}
			if (pose.orientation !== null) scope.quaternion.fromArray(pose.orientation);

			pose = null;

			scope.matrix.compose(scope.position, scope.quaternion, scope.scale);
			scope.matrix.premultiply(scope.standingMatrix);
			scope.matrixWorldNeedsUpdate = true;
			scope.visible = true;

			//  Thumbpad and Buttons.

			if (!_.isEqual(axes, gamepad.axes)) {
				let action = "";
				axes = gamepad.axes.map(x => x);
				if (axes[0] === 0 && axes[1] === 0) {
					action = "up"
				} else if (Math.abs(axes[0]) > Math.abs(axes[1]) && Math.abs(axes[0]) > 0.5) {
					action = axes[0] > 0 ? "right" : "left"
				} else if (Math.abs(axes[1]) > 0.5) {
					action = axes[1] > 0 ? "top" : "bottom"
				}
				//  X axis: -1 = Left, +1 = Right.
				//  Y axis: -1 = Bottom, +1 = Top.
				scope.dispatchEvent({ type: 'axischanged', axes: axes, action: action });

			}

			gamepad.buttons.forEach(function (button, index) {
				if (buttonsState[index] !== button.pressed) {
					buttonsState[index] = button.pressed;

					let action = buttonsState[index] ? 'down' : 'up';

					// console.log((buttonNames[index] || index) + action);

					if (buttonNames[index]) {
						scope.dispatchEvent({
							type: buttonNames[index] + action,
							axes: axes,
							index: index,
							pressed: buttonsState[index],
							action: action,
						});
					}

					scope.dispatchEvent({
						type: index + action,
						index: index,
						axes: axes,
						pressed: buttonsState[index],
						action: action,
					});
				}
			})


		} else {
			scope.visible = false;

		}


	};

};

THREE.VRController.prototype = Object.create(THREE.Object3D.prototype);
THREE.VRController.prototype.constructor = THREE.VRController;

export default THREE.VRController;
