import { Observable } from 'rxjs/Rx';
import * as THREE from 'three'
class VRGamepadControl {
    constructor(gamepad, cloudScene, mapContainer, callback) {
        this.gamepad = gamepad;
        this.mapContainer = mapContainer;
        this.meshs = []
        this.callback = callback || function (data) {
            console.error("Miss callback: ", data)
        }
        this.cloudScene = cloudScene
        // mouse over/drag controls
        this.plane = new THREE.Plane(new THREE.Vector3(0, 1, 0));
        this.raycaster = new THREE.Raycaster()
        this.raycaster.params.Points.threshold = 2 // make point detection tighter
        this.tempMatrix = new THREE.Matrix4();

        this.initPointerControl()

        this.initNormalControl()
    }

    initNormalControl() {
        Observable.merge(
            this.gamepad.rightTriggerStream.distinctUntilChanged().filter(x => x == 'down')
        ).map(() => {
            return this.nodeHitTest(true)
        })
            .distinctUntilChanged()
            .subscribe(intersectObject => {
                this.callback('right', 'Trigger', intersectObject)
            });

        this.gamepad.buttonAStream.distinctUntilChanged().filter(x => x)
            .subscribe(x => {
                this.callback('right', 'leftTriggerStream', [])
            });
        this.gamepad.buttonBStream.distinctUntilChanged().filter(x => x)
            .subscribe(x => {
                this.callback('right', 'leftTriggerStream', [])
            });
    }

    getIntersections(controller, onlyUSMap) {
        if (!this.mapContainer) {
            return [];
        }

        let meshs = [];
        if (this.mapContainer.state.layout.showUsMap || onlyUSMap) {
            meshs = this.mapContainer.usMap.children;
        } else {
            if (this.mapContainer.bars.group.visible) {
                meshs = this.mapContainer.bars.group.children;
            }
        }

        this.tempMatrix.identity().extractRotation(controller.matrixWorld);
        this.raycaster.ray.origin.setFromMatrixPosition(controller.matrixWorld);
        this.raycaster.ray.direction.set(0, 0, -1).applyMatrix4(this.tempMatrix);

        if (meshs && meshs.length > 0) {
            return this.raycaster.intersectObjects(meshs, true);
        } else {
            console.warn("Please check the intersectObjects meshs");
            return [];
        }

    }

    nodeHitTest(onlyUSMap) {
        let controller = this.gamepad.rightController
        let intersects = this.getIntersections(controller, onlyUSMap)
        if (intersects.length > 0) {
            let hitObj = intersects[0].object
            if (hitObj) {
                let nodePostion = hitObj.position.clone();
                // let dist = this.cloudScene.localToWorld(nodePostion)
                //     .distanceTo(controller.position.clone()
                //         .applyMatrix4(controller.standingMatrix))
                // // this.gamepad.updateLineLength(controller, dist)
            }
            return hitObj
        } else {
            // not an ideal place for this, but just to recover the pointer length
            // this.gamepad.updateLineLength(controller)
            return null
        }
    }

    doDragDrop(param) {
        let controller = this.gamepad.rightController
        // Move the node to where the controller is pointing now, but keep the distance
        let newPositionWorld = controller.position.clone();
        newPositionWorld.add((controller.getWorldDirection().multiplyScalar(-1 * param.dist)));
        newPositionWorld.applyMatrix4(controller.standingMatrix)
        let newPosition = this.cloudScene.worldToLocal(newPositionWorld)
    }

    initPointerControl() {
        let intersectStream = this.gamepad.rightControllerPoseStream
            .throttleTime(200)
            .map(() => {
                const intersectObject = this.nodeHitTest()
                return intersectObject
            })
            .distinctUntilChanged()
            .subscribe(intersectObject => {
                if (intersectObject) {
                    this.callback('right', 'MovePointer', intersectObject)
                } else {
                    this.callback('right', 'MovePointerNull')
                }
            })
    }
}

export default VRGamepadControl;