import _ from 'lodash'
import * as THREE from 'three'
import Rx from 'rxjs/Rx';

import VRDeviceModel from './model/VRDeviceModel'
import VRController from './VRController'
import WebVR from './WebVR'

class VRGampad {

    constructor(scene, renderer, audioManager) {

        this.scene = scene;
        this.renderer = renderer;
        this.audioManager = audioManager;

        //gamepad stream right
        this.rightControllerPoseStream = new Rx.Subject()
        this.rightGripStream = new Rx.Subject()
        this.rightTriggerStream = new Rx.Subject()
        this.buttonAStream = new Rx.Subject()
        this.buttonBStream = new Rx.Subject()

        this.rightThumbpadStream = new Rx.Subject();
        this.rightAxischangedStream = new Rx.Subject();

        //gamepad stream left
        this.leftControllerPoseStream = new Rx.Subject()
        this.leftGripStream = new Rx.Subject()
        this.leftTriggerStream = new Rx.Subject()
        this.buttonXStream = new Rx.Subject()
        this.buttonYStream = new Rx.Subject()

        this.leftThumbpadStream = new Rx.Subject();
        this.leftAxischangedStream = new Rx.Subject();

        this.triggerMoveStream = new Rx.Subject();

        this.standingMatrix = this.renderer.vr.getStandingMatrix();


        // this.triggerMoveStream
        //     // .debounceTime(50)
        //     .subscribe(x => {
        //         console.log("move :", x);
        //         if (x.name == 'right' && this.rightController) {
        //             this.trigger('triggermove', this.rightController);
        //         } else if (x.name == 'left' && this.leftController) {
        //             this.trigger('triggermove', this.leftController);
        //         }
        //     });

        this.controllerNames = ["left", "right", "camera"];
        this.events = [];

        //controller
        this.leftController = null;
        this.rightController = null;

        this.attachGamePad();
        window.addEventListener("gamepadconnected", (e) => {
            console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
                e.gamepad.index, e.gamepad.id,
                e.gamepad.buttons.length, e.gamepad.axes.length);

            this.attachGamePad()
        });

        window.addEventListener('vrdisplaypresentchange', (event) => {
            this.onVRDisplayPresentChange(event);
        });
    }

    onVRDisplayPresentChange(event) {
        console.log("====VR Display Present Changed=====")

        if (event.display.isPresenting) {
            console.log("===Entering VR===")
            this.attachGamePad()
        } else {
            console.log("===Leaving VR===")
            this.clear();
        }
        this.trigger("isPresenting", event);

    }

    getGamepadLine() {

        if (!this._gamePadLine) {
            let lineGeo = new THREE.Geometry();
            lineGeo.vertices.push(new THREE.Vector3(0, 0, 0));
            lineGeo.vertices.push(new THREE.Vector3(0, 0, - 1));
            let material = new THREE.LineBasicMaterial({
                color: window.globalVariable.laserColor
            });

            let line = new THREE.Line(lineGeo, material);
            line.name = 'line';
            line.scale.z = 4;
            this._gamePadLine = line;
        }

        return this._gamePadLine.clone();
    }

    updateLineLength(controller, length) {
        let line = controller.getObjectByName('line');
        if (!length) {
            line.scale.z = 4;
        } else {
            line.scale.z = length > 0 ? length : 1
        }
    }

    getControllers() {
        return {
            left: this.leftController,
            right: this.rightController
        }
    }

    attachGamePad() {

        if (!WebVR.isPresenting(this.renderer)) {
            return;
        }

        let that = this
        if (this.leftController && this.rightController) return //skip if gamepad already attached

        const gamepads = _.filter(navigator.getGamepads(), d => d !== null);
        // console.log("Current gamepads:", gamepads);
        _.each(gamepads, (gamepad) => {
            if (gamepad && gamepad.displayId > 0) { //must VR gamepad
                let index = gamepad.index;
                //fixed the gamepad id
                index = (/left/ig).test(gamepad.id) ? 0 : index;
                index = (/right/ig).test(gamepad.id) ? 1 : index;
                console.log("Current gamapad is:", gamepad.id, index, gamepad.displayId);

                let controller = null;
                //already init skip
                if ((index == 0 && !this.leftController) || (index != 0 && !this.rightController)) {

                    controller = this.initRiftController(gamepad.id, index);
                    // switch (gamepad.id.slice(0, 14)) {
                    //     case 'OpenVR Gamepad':
                    //         controller = this.initViveController(gamepad.id, index);
                    //         break;
                    //     case 'Oculus Touch (':
                    //         controller = this.initRiftController(gamepad.id, index);
                    //         break;
                    // }


                    if (controller) {
                        controller.type = gamepad.id;
                        controller.isOculus = (/Oculus/ig).test(gamepad.id);
                        controller.name = this.controllerNames[index];
                        let pointerLine = this.getGamepadLine();
                        controller.add(pointerLine);
                        controller.pointerLine = pointerLine
                        that.bindControllerEvents(controller, controller.name);
                        that.attachMenu(controller);

                        controller.standingMatrix = this.renderer.vr.getStandingMatrix();
                        controller.userData.points = [new THREE.Vector3(), new THREE.Vector3()];
                        controller.userData.matrices = [new THREE.Matrix4(), new THREE.Matrix4()];

                        controller.audio = this.audioManager ? this.audioManager.createAudio({
                            name: 'click_' + (index == 0 ? "left" : "right"),
                            type: 'positional',
                            source: '/public/audio/click_' + index + '.mp3',
                            refDistance: 0.1,
                            // playOverwrite: true,
                            volume: 1,
                        }) : null;

                        that.scene.add(controller);


                        if (index == 0) {
                            that.leftController = controller;
                            controller.pointerLine.visible = false // let's hide it first, only turn on when in searching mode

                        } else {
                            that.rightController = controller;
                            //  controller.pointerLine.visible = false // let's hide it first, only turn on when in searching mode
                        }
                    }


                } else {
                    console.warn("Already init gamepad controller, ingore it");
                }

            }

        });

        this.bindGripsAndTriggerEvents();
    }

    bindGripsAndTriggerEvents(isClean) {
        if (!this.__eventsCacheForGripsTriggerHandler)
            this.__eventsCacheForGripsTriggerHandler = {
                right: {

                    "gripsdown": (event) => this.rightGripStream.next('down'),
                    "gripsup": (event) => this.rightGripStream.next('up'),

                    "triggerdown": (event) => this.rightTriggerStream.next('down'),
                    "triggerup": (event) => this.rightTriggerStream.next('up'),

                    "menudown": (event) => this.buttonAStream.next(true),
                    "menuup": (event) => this.buttonAStream.next(false),

                    "4down": (event) => this.buttonBStream.next(true),
                    "4up": (event) => this.buttonBStream.next(false),

                    "thumbpaddown": (event) => this.rightThumbpadStream.next("down"),
                    "thumbpadup": (event) => this.rightThumbpadStream.next("up"),

                    "axischanged": (event) => {
                        // axes[0] = event.axes[0]; //  X axis: -1 = Left, +1 = Right.
                        // axes[1] = event.axes[1]; //  Y axis: -1 = Bottom, +1 = Top.
                        this.rightAxischangedStream.next(event.action)
                    },
                },
                left: {
                    "gripsdown": (event) => this.leftGripStream.next('down'),
                    "gripsup": (event) => this.leftGripStream.next('up'),

                    "triggerdown": (event) => this.leftTriggerStream.next('down'),
                    "triggerup": (event) => this.leftTriggerStream.next('up'),

                    "menudown": (event) => this.buttonXStream.next(true),
                    "menuup": (event) => this.buttonXStream.next(false),

                    "4down": (event) => this.buttonYStream.next(true),
                    "4up": (event) => this.buttonYStream.next(false),

                    "thumbpaddown": (event) => this.leftThumbpadStream.next(true),
                    "thumbpadup": (event) => this.leftThumbpadStream.next(false),

                    "axischanged": (event) => {
                        // axes[0] = event.axes[0]; //  X axis: -1 = Left, +1 = Right.
                        // axes[1] = event.axes[1]; //  Y axis: -1 = Bottom, +1 = Top.
                        this.leftAxischangedStream.next(event.action)

                    },


                }
            }
        // right
        if (this.rightController) {
            _.map(this.__eventsCacheForGripsTriggerHandler.right, (eventHandler, eventName) => {
                this.rightController.removeEventListener(eventName, eventHandler);
                if (!isClean) {
                    this.rightController.addEventListener(eventName, eventHandler);
                    // console.log("bind event ", eventName, eventHandler)
                }
            })

        } else {
            console.log("Right Controller is null")
        }
        // left
        if (this.leftController) {
            _.map(this.__eventsCacheForGripsTriggerHandler.left, (eventHandler, eventName) => {
                this.leftController.removeEventListener(eventName, eventHandler);

                if (!isClean) {
                    this.leftController.addEventListener(eventName, eventHandler);
                    // console.log("bind event ", eventName, eventHandler)
                }
            })
        } else {
            console.log("Left Controller is null")
        }

        // this.buttonAStream.distinctUntilChanged().subscribe(x=>console.log('A', x))
        // this.buttonBStream.distinctUntilChanged().subscribe(x=>console.log('B', x))
        // this.buttonXStream.distinctUntilChanged().subscribe(x=>console.log('X', x))
        // this.buttonYStream.distinctUntilChanged().subscribe(x=>console.log('Y', x))
    }

    bindControllerEvents(controller, name, isClean) {
        if (!controller) {
            return console.warn("Miss controller, can't bind the events,");
        }

        //init events cache 
        name = name ? name : controller.name;
        if (!this._eventsCacheHandler) {
            this._eventsCacheHandler = {};
        }

        if (!this._eventsCacheHandler[name]) {
            this._eventsCacheHandler[name] = {};
        }


        const events = [
            'axischanged',
            'thumbpaddown',
            'thumbpadup',
            'triggerdown',
            'triggerup',
            'gripsdown',
            'gripsup',
            'menudown',
            'menuup',
            '4down',
            '4up'
        ]

        //cache events function

        events.map((eventName) => {

            if (!this._eventsCacheHandler[name][eventName]) {
                this._eventsCacheHandler[name][eventName] = (event) => {
                    // console.warn("Event listen >>>> ", eventName, event);
                    this.trigger(eventName, event);

                }
            }

            // Rx.Observable.fromEvent(this, eventName)
            // .subscribe(x=>console.log("************", eventName, x))

            if (!isClean) {
                controller.addEventListener(eventName, this._eventsCacheHandler[name][eventName], false);
                // console.log("bind event ", eventName, this._eventsCacheHandler[name][eventName])
            } else {
                controller.removeEventListener(eventName, this._eventsCacheHandler[name][eventName], false);
            }

        })

    }

    clear() {
        //clear bind event
        _.each(this.getControllers(), (c, cName) => {
            if (c) {
                this.bindControllerEvents(c, null, true);
                this.scene.remove(c);
            }

        });

        this.rightController = null;
        this.leftController = null;

        this.bindGripsAndTriggerEvents(true);
    }

    update() {

        _.each(this.getControllers(), (c) => {
            if (c) {
                c.update();


                if (c.getGamepad()) { //trigger

                    let gamaped = c.getGamepad();
                    if (c.name == 'left') {
                        this.leftControllerPoseStream.next(gamaped.pose)
                    } else if (c.name == 'right') {//grips
                        this.rightControllerPoseStream.next(gamaped.pose)

                    }

                    // if (c.name == 'left' && c.getButtonState(2) && this.rightController && this.rightController.getButtonState(2)) {//grips
                    //     this.leftControllerPoseStream.next(gamaped.pose)
                    // } else if (c.name == 'right' && c.getButtonState(2) && this.leftController && this.leftController.getButtonState(2)) {//grips
                    //     this.rightControllerPoseStream.next(gamaped.pose)

                    // } else if (c.getButtonState(1)) {

                    //     this.trigger('triggermove', c);
                    //     // this.triggerMoveStream.next({
                    //     //     name: c.name,
                    //     //     position: gamaped.pose.position
                    //     // });
                    // }

                    gamaped = null;
                }


            }

        });
    }


    attachMenu(controller) {
        switch (controller.name) {
            case 'right':

                break;
        }
    }

    on(eventName, callback) {
        this.events[eventName] = callback;
    }

    trigger(eventName, data) {
        //when the  button down event , play the audio
        if ((/down$/ig).test(eventName) && data.target.audio) {
            if (data.target.audio && data.target.audio.play)
                data.target.audio.play();
        }

        if (this.events[eventName]) {
            this.events[eventName](data);
        }
        // else {
        //     console.log('No one is listening to ' + name + '. :(');
        // }

    }



    // isEqualArray(aArray, bArray, accuracy = 0.01) {
    // 	if (aArray == bArray) {
    // 		return true;
    // 	} else if (aArray && bArray && aArray.length >= 0 && bArray.length >= 0 && aArray.length == bArray.length) {
    // 		let resArray = aArray.map((e, index) => {
    // 			return Math.abs(e - bArray[index]) < accuracy;
    // 		})
    // 		return (resArray.filter(isEqual => !isEqual)).length == 0;
    // 	} else {
    // 		return false;
    // 	}

    // }

    updateGamepadPose(leftPose, rightPose) {
        // let hasChange = false;
        // if (leftPose
        // 	&& (!this.isEqualArray(this.gamepadPose.left.position, leftPose.position) || !this.isEqualArray(this.gamepadPose.left.orientation, leftPose.orientation))
        // ) {
        // 	this.gamepadPose.left.position = leftPose.position;
        // 	this.gamepadPose.left.orientation = leftPose.orientation;
        // 	hasChange = true;
        // }

        // if (rightPose && (!this.isEqualArray(this.gamepadPose.right.position, rightPose.position)
        // 	|| !this.isEqualArray(this.gamepadPose.right.orientation, rightPose.orientation))) {
        // 	this.gamepadPose.right.position = rightPose.position;
        // 	this.gamepadPose.right.orientation = rightPose.orientation;
        // 	hasChange = true;
        // }

        // if (hasChange) {
        // 	console.log("GamePad pose changed :", this.gamepadPose);
        // }

    }


    /**
     * Init Device
     */

    initViveController(name, index) {

        let controller = new VRController(index);

        this.getViveControllerModel((object) => {
            controller.add(object.clone());
        });

        return controller;
    }

    initRiftController(name, index) {
        let controller = new VRController(name);

        let hand = null;
        if (name.toLowerCase().search("left") !== -1) {
            hand = "left";
        }
        if (name.toLowerCase().search("right") !== -1) {
            hand = "right";
        }

        if (!hand) {
            if (this.rightController) {
                hand = 'left'
            } else {
                hand = "right";
            }
        }

        this.getRiftControllerModel(hand, (object) => {
            controller.add(object.clone());
        });

        return controller;

    }


    getViveControllerModel(callback) {
        if (this._viveControllerModel) {
            callback(this._viveControllerModel);
        } else {
            this._viveControllerModel = VRDeviceModel.getViveControllerModel(callback);
        }
    }

    getRiftControllerModel(type, callback) {
        switch (type) {
            case "left": {

                if (this._riftLeftControllerModel) {
                    callback(this._riftLeftControllerModel);
                } else {
                    this._riftLeftControllerModel = VRDeviceModel.getRiftControllerModel(type, callback);

                }

                break;
            }

            case "right": {
                if (this._riftRightControllerModel) {
                    callback(this._riftRightControllerModel);
                }
                else {
                    this._riftRightControllerModel = VRDeviceModel.getRiftControllerModel(type, callback)
                }
                break;
            }
        }
    }

}

export default VRGampad
export { VRGampad }