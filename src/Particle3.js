import React, { Component } from 'react';
import * as THREE from 'three';
import Mousetrap from 'mousetrap';
// import * as  TWEEN from 'es6-tween'

import TWEEN from '@tweenjs/tween.js'


import earth from './map';
// import earth from './earth';
// import earth from './earth_new';
import logo from './logo';


class Particle extends Component {
    constructor(scene) {
        super()
        this.scene = scene;
        this.initParticle()
        this.isComplete = true;
        this.startChange = false;
        this.finishChange = false;
        this.logoTween = []
        this.earthTween = []
        this.getRandomPosition()
        Mousetrap.bind('space', () => {
            this.handleParticle();
        })
    }

    getRandomPosition() {
        this.randomPosition = []
        earth.forEach(n =>
            this.randomPosition.push({
                x: 20 * (Math.random() - Math.random()),
                y: 20 * (Math.random() - Math.random()),
                z: 20 * (Math.random() - Math.random()),
            })
        )
    }

    handleParticle() {
        if (this.isComplete) {
            this.setRandomToLogo()
            this.setPointToRandom()
            this.isComplete = false;
            this.startChange = true
        } else {
            this.logoTween.forEach((n, i) => {
                if (this.logoTween[i]) {
                    this.logoTween[i].stop();
                }
            })
            this.earthTween.forEach((n, i) => {
                if (this.logoTween[i]) {
                    this.earthTween[i].stop();
                }
            })
            this.logoTween = []
            this.earthTween = []
            this.cloud.geometry.vertices.forEach((pos, idx) => {
                this.cloud.geometry.vertices[idx].x = earth[idx].X / -200;
                this.cloud.geometry.vertices[idx].y = earth[idx].Y / 200;
                this.cloud.geometry.vertices[idx].z = earth[idx].Z / 200;
                this.cloud.geometry.verticesNeedUpdate = true;
            });
            this.isComplete = false;
            this.startChange = true;
            this.setRandomToLogo()
            this.setPointToRandom()
        }
    }

    initParticle() {
        var geom = new THREE.Geometry();
        var materialGeo = new THREE.PointCloudMaterial({ size: 1 / 50, vertexColors: true, color: 0xffffff });
        for (let i = 0; i < earth.length - 1; i++) {
            var particle = new THREE.Vector3(earth[i].X / 50, earth[i].Z / 50, earth[i].Y / 50);
            geom.vertices.push(particle);
            geom.colors.push(new THREE.Color(0xffffff));
        }
        this.cloud = new THREE.PointCloud(geom, materialGeo);
        // this.cloud.rotation.x = 0.2;
        this.scene.add(this.cloud);
    }

    setRandomToLogo() {
        this.finishChange = false
        this.cloud.geometry.vertices.forEach((pos, idx) => {
            let randomPosition = ((logo.length - 1) * Math.random()).toFixed(0)
            this.logoTween[idx] = new TWEEN.Tween({
                x: this.randomPosition[idx].x,
                y: this.randomPosition[idx].y,
                z: this.randomPosition[idx].z,
                // rotation: this.cloud.rotation.y
            })
                .to({
                    x: logo[idx] ? logo[idx].X / 300 : logo[randomPosition].X / 300,
                    y: logo[idx] ? logo[idx].Y / 300 : logo[randomPosition].Y / 300,
                    z: logo[idx] ? logo[idx].Z / 300 : logo[randomPosition].Z / 300,
                    // rotation: -1 * Math.PI
                }, 3000)
                .easing(TWEEN.Easing.Quartic.InOut)
                .onUpdate((position) => {
                    this.cloud.geometry.vertices[idx].x = position.x;
                    this.cloud.geometry.vertices[idx].y = position.y;
                    this.cloud.geometry.vertices[idx].z = position.z;
                    // this.cloud.rotation.y = position.rotation;
                    this.cloud.geometry.verticesNeedUpdate = true;
                })
                .onComplete(() => {
                    this.logoTween = []
                    this.finishChange = true
                })
        })
    }
    // setRandomToLogo() {
    //     this.finishChange = false
    //     this.cloud.geometry.vertices.forEach((pos, idx) => {
    //         let randomPosition = ((logo.length - 1) * Math.random()).toFixed(0)
    //         this.logoTween[idx] = new TWEEN.Tween({
    //             x: this.randomPosition[idx].x,
    //             y: this.randomPosition[idx].y,
    //             z: this.randomPosition[idx].z,
    //             // rotation: this.cloud.rotation.y
    //         })
    //             .to({
    //                 x: logo[idx] ? logo[idx].X / 300 : logo[randomPosition].X / 300,
    //                 y: logo[idx] ? logo[idx].Y / 300 : logo[randomPosition].Y / 300,
    //                 z: logo[idx] ? logo[idx].Z / 300 : logo[randomPosition].Z / 300,
    //                 // rotation: -1 * Math.PI
    //             }, 3000)
    //             .easing(TWEEN.Easing.Quartic.InOut)
    //             .onUpdate((position) => {
    //                 this.cloud.geometry.vertices[idx].x = position.x;
    //                 this.cloud.geometry.vertices[idx].y = position.y;
    //                 this.cloud.geometry.vertices[idx].z = position.z;
    //                 // this.cloud.rotation.y = position.rotation;
    //                 this.cloud.geometry.verticesNeedUpdate = true;
    //             })
    //             .onComplete(() => {
    //                 this.logoTween = []
    //                 this.finishChange = true
    //             })
    //     })
    // }

    setPointToRandom() {
        this.cloud.geometry.vertices.forEach((pos, idx) => {
            this.earthTween[idx] = new TWEEN.Tween({
                x: pos.x,
                y: pos.y,
                z: pos.z,
                rotation: this.cloud.rotation.y
            })
                .to({
                    x: this.randomPosition[idx].x,
                    y: this.randomPosition[idx].y,
                    z: this.randomPosition[idx].z,
                    // rotation: this.cloud.rotation.y - 2 * Math.PI
                }, 3000)
                .onUpdate((position) => {
                    this.cloud.geometry.vertices[idx].x = position.x;
                    this.cloud.geometry.vertices[idx].y = position.y;
                    this.cloud.geometry.vertices[idx].z = position.z;
                    // this.cloud.rotation.y = position.rotation;
                    this.cloud.geometry.verticesNeedUpdate = true
                })
                .easing(TWEEN.Easing.Quartic.InOut)
                .onComplete((position) => {
                    this.earthTween = []
                })
        });
    }

    update() {
        TWEEN.update();
    //     while (this.cloud.rotation.y < 0) {
    //         this.cloud.rotation.y = this.cloud.rotation.y + 2 * Math.PI
    //     }
    //     if (!this.finishChange) {
    //         this.cloud.rotation.y -= 0.01;
    //     } else {
    //         if (-0.01 <= this.cloud.rotation.y && this.cloud.rotation.y <= 0.02) {
    //             this.cloud.rotation.y = 0;
    //         } else {
    //             this.cloud.rotation.y -= 0.01;
    //         }
    //     }
    //     if (this.startChange && 5 / 4 * Math.PI <= this.cloud.rotation.y && this.cloud.rotation.y <= 3 / 2 * Math.PI) {
    //         this.startChange = false
    //         this.earthTween.forEach((n, index) => {
    //             n.chain(this.logoTween[index])
    //             n.start()
    //         })
    //     }
    //     if (this.startChange) {
    //         this.cloud.rotation.y -= 0.02;
    //     }
    }
}
export default Particle