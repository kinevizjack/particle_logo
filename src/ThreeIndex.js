import React, { Component } from 'react';

import {WebVR} from './vr/WebVR'
import VRGampad from './vr/VRGampad'
import VRGampadControl from './vr/VRGamepadControl'




let THREE = window.THREE
class ThreeIndex extends Component {
    constructor(props, context) {
        super(props, context);
        THREE.Cache.enabled = true;
        this.container = document.createElement('div');


        
        



        // this.camera
        // this.cameraTarget

        this.camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 1500);
        this.camera.position.set(0, 0, 100);
        this.cameraTarget = new THREE.Vector3(0, 0, 0);
        this.scene = new THREE.Scene();
        window.scene = this.scene;
        this.group = new THREE.Group();


        this.mapGroup = new THREE.Group();
        this.mapGroup.scale.set(0.01,0.01,0.01)
        this.scene.add(this.mapGroup)

        this.init();
        this.animate();
    }

    updateMaterial(options) {
        return new THREE.MeshPhongMaterial({
            color: options.color || 0xFFFFFF,
            wireframe: options.wireframe || false,
            opacity: options.opacity || 0.75,
            transparent: options.transparent || false,
            side: THREE.DoubleSide,
            depthTest: options.depthTest || false,
        });
    }

    loadObj(url, options, cb) {
        new THREE.OBJLoader(new THREE.LoadingManager()).load(url, (object) => {
            object.traverse((child) => {
                child.position.set(0, 0, 0); // force center
                if (child instanceof THREE.Mesh) {
                    child.material = this.updateMaterial(options);
                }
            });
            cb(object);
        }, (xhr) => {
            if (xhr.lengthComputable) {
                const percentComplete = (xhr.loaded / xhr.total) * 100;
                console.log('Obj ', Math.round(percentComplete, 2) + '% downloaded');
            }
        }, (xhr) => {
            console.log('OBJLoader Error: ', xhr);
        });
    }

    init() {
        document.body.appendChild(this.container);
        this.dirLight = new THREE.DirectionalLight(0xffffff, 0.125);
        this.dirLight.position.set(0, 1000, 1000)//.normalize();
        this.scene.add(this.dirLight);
        this.pointLight = new THREE.PointLight(0xffffff, 1.5);
        this.pointLight.position.set(0, 1000, 900);
        this.scene.add(this.pointLight);
        this.pointLight.color.setHex(0xffffff);

        let cubeGeometry = new THREE.BoxGeometry(4, 4, 4)
        let cubeMaterial = new THREE.MeshLambertMaterial({ color: 0xffffff })
        let cube = new THREE.Mesh(cubeGeometry, cubeMaterial)
        this.scene.add(cube)


        this.loadObj('./rightController.obj', {}, (laptopShape) => {
            laptopShape.scale.set(100, 100, 100);
            laptopShape.traverse((child) => {
                if (child instanceof THREE.Mesh) {
                    child.material = new THREE.MeshLambertMaterial({
                        color: 0xffffff,
                        side: THREE.DoubleSide,
                        transparent: true,
                        opacity: 1,
                    });
                    child.material.needsUpdate = true;
                }
            });
            laptopShape.position.x = -30;
            laptopShape.position.y = 0
            laptopShape.position.z = -8;
            laptopShape.visible = true;
            laptopShape.name = 'laptop-shape-3d';
            this.scene.add(laptopShape);
        });
        this.loadObj('./leftController.obj', {}, (laptopShape) => {
            laptopShape.scale.set(100, 100, 100);
            laptopShape.traverse((child) => {
                if (child instanceof THREE.Mesh) {
                    child.material = new THREE.MeshLambertMaterial({
                        color: 0xffffff,
                        side: THREE.DoubleSide,
                        transparent: true,
                        opacity: 1,
                    });
                    child.material.needsUpdate = true;
                }
            });
            laptopShape.position.x = 0;
            laptopShape.position.y = 0
            laptopShape.position.z = -8;
            laptopShape.visible = true;
            laptopShape.name = 'laptop-shape-3d';
            this.scene.add(laptopShape);
        });


        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.container.appendChild(this.renderer.domElement);

        this.webVR =  new WebVR(this.renderer)
        window.webVR = this.webVR
    }




    animate() {
        requestAnimationFrame(this.animate.bind(this));
        this.render();
    }

    render() {
        this.camera.lookAt(this.cameraTarget);
        this.renderer.clear();
        this.renderer.render(this.scene, this.camera);
        return (
            <div>test</div>
        )
    }
}

export default ThreeIndex;
