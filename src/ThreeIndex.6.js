import React, { Component } from 'react';
import TextShape from './TextShape';


let THREE = window.THREE
class ThreeIndex extends Component {
    constructor(props, context) {
        super(props, context);
        THREE.Cache.enabled = true;
        this.container = document.createElement('div');
        // this.camera
        // this.cameraTarget

        this.camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 1500);
        this.camera.position.set(0, 0, 100);
        this.cameraTarget = new THREE.Vector3(0, 0, 0);
        this.scene = new THREE.Scene();
        // this.renderer;
        this.group = new THREE.Group();
        // this.textMesh1
        // this.textGeo
        // this.materials;
        // this.text = "three.js"
        // this.height = 20
        // this.size = 70
        // this.hover = 30
        // this.curveSegments = 4
        // this.bevelThickness = 2
        // this.bevelSize = 1.5
        // this.bevelSegments = 3
        // this.bevelEnabled = true
        // this.font = undefined
        // this.fontName = "optimer" // helvetiker, optimer, gentilis, droid sans, droid serif
        // this.fontWeight = "bold"; // normal bold
        this.init();
        this.animate();
    }

    init() {
        document.body.appendChild(this.container);
        this.dirLight = new THREE.DirectionalLight(0xffffff, 0.125);
        this.dirLight.position.set(0, 0, 1)//.normalize();
        this.scene.add(this.dirLight);
        this.pointLight = new THREE.PointLight(0xffffff, 1.5);
        this.pointLight.position.set(0, 100, 90);
        this.scene.add(this.pointLight);
        this.pointLight.color.setHex(0xffffff);

        // this.group.position.y = 100;
        // this.scene.add(this.group);

        // this.loadFont();

        this.TextShape = new TextShape()

        let shape = this.TextShape.getMesh()
        shape.position.y = 0
        this.scene.add(shape)


        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.container.appendChild(this.renderer.domElement);
    }


    loadFont() {
        this.loader = new THREE.FontLoader();
        this.loader.load('fonts/' + this.fontName + '_' + this.fontWeight + '.typeface.json', (response) => {
            this.font = response;
            this.refreshText();
        });
    }
    createText() {

        this.textGeo = new THREE.TextBufferGeometry(this.text, {

            font: this.font,
            size: this.size,
            height: this.height,
            curveSegments: this.curveSegments,

            bevelThickness: this.bevelThickness,
            bevelSize: this.bevelSize,
            bevelEnabled: this.bevelEnabled,

            material: 0,
            extrudeMaterial: 1

        });

        this.textGeo.computeBoundingBox();
        this.textGeo.computeVertexNormals();

        this.centerOffset = -0.5 * (this.textGeo.boundingBox.max.x - this.textGeo.boundingBox.min.x);

        this.textMesh1 = new THREE.Mesh(this.textGeo, this.materials);

        this.textMesh1.position.x = this.centerOffset;
        this.textMesh1.position.y = this.hover;
        this.textMesh1.position.z = 0;

        this.textMesh1.rotation.x = 0;
        this.textMesh1.rotation.y = Math.PI * 2;

        this.group.add(this.textMesh1);
    }

    refreshText() {
        this.group.remove(this.textMesh1);
        if (!this.text) return;
        this.createText();
    }

    animate() {
        requestAnimationFrame(this.animate.bind(this));
        this.render();
    }

    render() {
        this.camera.lookAt(this.cameraTarget);
        this.renderer.clear();
        this.renderer.render(this.scene, this.camera);
        return (
            <div>test</div>
        )
    }
}

export default ThreeIndex;
