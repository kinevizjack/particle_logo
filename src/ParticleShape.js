import React, { Component } from 'react';
import * as THREE from 'three';
import Mousetrap from 'mousetrap';
import TWEEN from 'tween.js'

// import LaptopCPU from './LaptopCPU';
// import LaptopFeet from './LaptopFeet';
// import LaptopKeys from './LaptopKeys';
// import LaptopMonitor from './LaptopMonitor';
// import LaptopScreen from './LaptopScreen';
// import IphoneBody from './IphoneBody';
// import IphoneButtons from './IphoneButtons';
// import IphoneCamera from './IphoneCamera';
// import IphoneScreen from './IphoneScreen';

// import TableBody from './TableBody';
// import TableButtons from './TableButtons';
import TableBody from './TableBody';
import TableButtons from './TableButtons';

// TableBody
// TableButtons
// TableBody
// TableButtons

// import Laptop from './Laptop';



// LaptopCPU
// LaptopFeet
// LaptopKeys
// LaptopMonitor
// LaptopScreen
// LaptopTouchPad


// './IphoneBody';
// IphoneButtons
// IphoneCamera
// IphoneScreen

// LaptopCPU
// LaptopFeet
// LaptopKeys
// LaptopMonitor
// LaptopScreen
// LaptopTouchPad
// TableBody
// TableButtons
// TableBody
// TableButtons



class ParticleShape extends Component {
    constructor(scene) {
        super()
        this.scene = scene;
        this.group = new THREE.Group();
        this.scene.add(this.group)
        this.initParticle()
        this.initParticleShape()

        Mousetrap.bind('p', () => {
            this.setMapToEarth('p');
        })
    }

    changeShape(type, data) {

        let shape = this.scene.getObjectByName(type)
        if (data.position) {
            shape.position.x = data.position.x ? data.position.x : shape.position.x
            shape.position.y = data.position.y ? data.position.y : shape.position.y
            shape.position.z = data.position.z ? data.position.z : shape.position.z
        }
        if (data.rotation) {
            shape.rotation.x = data.rotation.x ? data.rotation.x * Math.PI : shape.rotation.x
            shape.rotation.y = data.rotation.y ? data.rotation.y * Math.PI : shape.rotation.y
            shape.rotation.z = data.rotation.z ? data.rotation.z * Math.PI : shape.rotation.z
        }
        if (data.scale) {
            shape.scale.set(data.scale, data.scale, data.scale)
        }
    }


    initParticle() {
        window.scene = this.scene
    }

    // TableBody
    // TableButtons
    // TableBody
    // TableButtons

    initParticleShape() {
        let Shapes = [
            {
                type: 'phone',
                name: 'TableBody',
                data: TableBody,
                position: [0, 0, 0],
                Rx: .42,
                color: 0xff0000
            },
            {
                type: 'phone',
                name: 'TableButtons',
                data: TableButtons,
                position: [-2.75, -1.3, 0],
                Rx: 1,
                color: 0x00ff00
            }
        ]


        Shapes.forEach(n => {
            let Shape = n.data
            let geom = new THREE.Geometry();
            let materialGeo = new THREE.PointCloudMaterial({ size: 1 / 50, vertexColors: true, color: n.color });
            for (let i = 0; i < Shape.length - 1; i++) {
                let particle = null;
                particle = new THREE.Vector3(Shape[i].X / 150, Shape[i].Y / 150, Shape[i].Z / 150);
                geom.vertices.push(particle);
                geom.colors.push(new THREE.Color(n.color));
            }
            this.cloud = new THREE.PointCloud(geom, materialGeo);
            this.cloud.name = n.name;
            this.cloud.position.x = n.position[0]
            this.cloud.position.y = n.position[1]
            this.cloud.position.z = n.position[2]
            this.cloud.rotation.z = n.Rx * Math.PI;
            this.group.add(this.cloud)
        })
    }

    setMapToEarth() {
        let sourcePosition = []
        let targetPosition = []
        this.group.children.forEach((n, i) => {
            this.group.children[i].scale.set(0.001, 0.001, 0.001)
            targetPosition[i] = {
                positionX: n.position.x,
                positionY: n.position.y,
                positionZ: n.position.z,
            }
            sourcePosition[i] = {
                positionX: Math.random() * 10 * (Math.random() > 0.5 ? 1 : -1),
                positionY: Math.random() * 10 * (Math.random() > 0.5 ? 1 : -1),
                positionZ: Math.random() * 10 * (Math.random() > 0.5 ? 1 : -1)
            }
        })
        new TWEEN.Tween()
            .to({
            }, 3000)
            .easing(TWEEN.Easing.Quadratic.In)
            .onUpdate((data) => {
                this.group.children.forEach((n, i) => {
                    n.position.x = targetPosition[i].positionX * data + sourcePosition[i].positionX * (1 - data)
                    n.position.y = targetPosition[i].positionY * data + sourcePosition[i].positionY * (1 - data)
                    n.position.z = targetPosition[i].positionZ * data + sourcePosition[i].positionZ * (1 - data)
                    this.group.children[i].scale.set(data * 2, data * 2, data * 2)
                })
                this.group.rotation.x = -0.35 * Math.PI * data
            })
            .onComplete(() => {
                console.log(targetPosition)
            })
            .start()
    }
    update() {
        // this.group.children.forEach(n => {
            // n.rotation.x += 0.01
        // })
        TWEEN.update();
    }
}

export default ParticleShape