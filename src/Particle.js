import React, { Component } from 'react';
import * as THREE from 'three';
import Mousetrap from 'mousetrap';
import TWEEN from 'tween.js'

import map from './LaptopCPU';
// './IphoneBody';
// IphoneButtons
// IphoneCamera
// IphoneScreen
// LaptopCPU
// LaptopFeet
// LaptopKeys
// LaptopMonitor
// LaptopScreen
// LaptopTouchPad
// TableBody
// TableButtons
// TableBody
// TableButtons

import earth from './earth';
import logo from './logo';




class Particle extends Component {
    constructor(scene) {
        super()
        this.scene = scene;
        this.initParticle()
        this.earthTween = []
        this.mapTween = []
        this.logoTween = []
        // this.getRandomPosition()
        this.isEarth = false;
        Mousetrap.bind('1', () => {
            this.goToEarth();
        })
        Mousetrap.bind('2', () => {
            this.goToLogo();
        })
    }

    // getRandomPosition() {
    //     this.randomPosition = []
    //     map.forEach(n =>
    //         this.randomPosition.push({
    //             x: 20 * (Math.random() - Math.random()),
    //             y: 20 * (Math.random() - Math.random()),
    //             z: 20 * (Math.random() - Math.random()),
    //         })
    //     )
    // }

    goToEarth() {
        this.setEarthtoLogo()
        this.setMapToEarth()
        this.earthTween.forEach((n, index) => {
            // n.chain(this.logoTween[index])
            n.start()
        })
    }

    goToLogo() {
        this.logoTween.forEach((n, index) => {
            n.start()
        })
    }

    initParticle() {
        let geom = new THREE.Geometry();
        let materialGeo = new THREE.PointCloudMaterial({ size: 1/50, vertexColors: true, color: 0xff0000 });
        for (let i = 0; i < map.length - 1; i++) {
            let randomPosition = ((map.length - 1) * Math.random()).toFixed(0)
            let particle = null;

            particle = new THREE.Vector3(map[i].X / 160, map[i].Y / 160, map[i].Z / 160);
            geom.vertices.push(particle);
            geom.colors.push(new THREE.Color(0xff0000));
        }
        this.cloud = new THREE.PointCloud(geom, materialGeo);

        this.scene.add(this.cloud);

        window.scene = this.scene
    }

    setMapToEarth() {
        this.cloud.geometry.vertices.forEach((pos, idx) => {
            let randomPosition = ((earth.length - 1) * Math.random()).toFixed(0)
            let position = {
                x: this.cloud.geometry.vertices[idx].x,
                y: this.cloud.geometry.vertices[idx].y,
                z: this.cloud.geometry.vertices[idx].z,
                rotation: 0
            }
            this.earthTween[idx] = new TWEEN.Tween(position)
                .to({
                    x: earth[idx] ? earth[idx].X / (-300) : earth[randomPosition].X / 300,
                    y: earth[idx] ? earth[idx].Y / 300 : earth[randomPosition].Y / 300,
                    z: earth[idx] ? earth[idx].Z / 300 : earth[randomPosition].Z / 300,
                    rotation: -1 * Math.PI
                }, 3000)
                .easing(TWEEN.Easing.Quadratic.In)
                .onUpdate(() => {
                    this.cloud.geometry.vertices[idx].x = position.x;
                    this.cloud.geometry.vertices[idx].y = position.y;
                    this.cloud.geometry.vertices[idx].z = position.z;
                    this.cloud.rotation.y = position.rotation;
                    this.cloud.geometry.verticesNeedUpdate = true;
                })
                .onComplete(() => {
                    this.earthTween = [];
                    this.isEarth = true;
                })
        })
    }
    setEarthtoLogo() {
        this.cloud.geometry.vertices.forEach((pos, idx) => {
            let randomPosition = ((logo.length - 1) * Math.random()).toFixed(0)
            let position = {
                x: earth[idx].X / (-300),
                y: earth[idx].Y / 300,
                z: earth[idx].Z / 300,
                // rotation: this.cloud.rotation.y
            }
            this.logoTween[idx] = new TWEEN.Tween(position)
                .to({
                    x: logo[idx] ? logo[idx].X / 350 : logo[randomPosition].X / 350,
                    y: logo[idx] ? logo[idx].Y / 350 : logo[randomPosition].Y / 350,
                    z: logo[idx] ? logo[idx].Z / 350 : logo[randomPosition].Z / 350,
                    // rotation: -1 * Math.PI
                }, 3000)
                .easing(TWEEN.Easing.Quartic.InOut)
                .onUpdate(() => {
                    this.cloud.geometry.vertices[idx].x = position.x;
                    this.cloud.geometry.vertices[idx].y = position.y;
                    this.cloud.geometry.vertices[idx].z = position.z;
                    // this.cloud.rotation.y = position.rotation;
                    this.cloud.geometry.verticesNeedUpdate = true;
                })
                .onComplete(() => {
                    this.logoTween = []
                    this.finishChange = true;
                    this.isEarth = false;
                })
        })
    }

    update() {
        TWEEN.update();
        if (this.isEarth) {
            this.cloud.rotation.y -= 0.01;
        }
        while (this.cloud.rotation.y < 0) {
            this.cloud.rotation.y = this.cloud.rotation.y + 2 * Math.PI
        }
        if (this.finishChange) {
            if (-0.01 <= this.cloud.rotation.y && this.cloud.rotation.y <= 0.02) {
                this.cloud.rotation.y = 0;
            } else {
                this.cloud.rotation.y -= 0.01;
            }
        }
        this.cloud.rotation.y -= 0.01;
    }
}
export default Particle