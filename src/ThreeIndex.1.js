import React, { Component } from 'react';
import * as THREE from 'three';
import Mousetrap from 'mousetrap';

import ParticleShape from './ParticleShape'
import Particle from './Particle'
import PanelHandle from './PanelHandle'
import CouplePanelHandle from './CouplePanelHandle'


class ThreeIndex extends Component {
    constructor(props, context) {
        super(props, context);
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer();
        this.initThree();
        this.animate();
        this.textureIndex = 0
        this.textureIndexLeft = 0
        this.textureIndexRight = 0
        Mousetrap.bind('up', () => {
            this.handlePanel('up');
        })
        Mousetrap.bind('left', () => {
            this.handlePanel('left');
        })
        Mousetrap.bind('right', () => {
            this.handlePanel('right');
        })
        Mousetrap.bind('down', () => {
            this.handlePanel('down');
        })
    }

    handlePanel(type) {
        switch (type) {
            case 'up': {
                this.textureIndex += 1;
                this.PanelHandle.handlePanel(this.textureIndex);
                break;
            }
            case 'left': {
                this.textureIndexLeft += 1;
                this.CouplePanelHandle.handlePanel(this.textureIndexLeft, this.textureIndexRight);
                break;
            }
            case 'right': {
                this.textureIndexRight += 1;
                this.CouplePanelHandle.handlePanel(this.textureIndexLeft, this.textureIndexRight);
                break;
            }
            case 'down': {
                this.textureIndexLeft += 1;
                this.textureIndexRight += 1;
                this.CouplePanelHandle.handlePanel(this.textureIndexLeft, this.textureIndexRight);
                break;
            }
        }
    }

    initThree() {
        this.renderer.setSize(window.innerWidth, 2 * window.innerHeight / 3);
        document.body.appendChild(this.renderer.domElement);
        window.scene = this.scene
        this.camera.position.z = 5;
        this.ParticleShape = new ParticleShape(this.scene)
        // this.Particle = new Particle(this.scene)
        // this.PanelHandle = new PanelHandle(this.scene)
        // this.CouplePanelHandle = new CouplePanelHandle(this.scene)
    }

    animate() {
        this.ParticleShape.update();
        requestAnimationFrame(this.animate.bind(this));
        this.renderer.render(this.scene, this.camera);
    };

    handleParticle(e, type, shape) {
        let data = {
            position: {},
            rotation: {}
        }

        switch (type) {
            case 'Px':
                data.position.x = e.target.value
                break
            case 'Py':
                data.position.y = e.target.value
                break
            case 'Pz':
                data.position.z = e.target.value
                break
            case 'Rx':
                data.rotation.x = e.target.value
                break
            case 'Ry':
                data.rotation.y = e.target.value
                break
            case 'Rz':
                data.rotation.z = e.target.value
                break
            case 'Scale':
                data.scale = e.target.value
                break

        }

        this.ParticleShape.changeShape(shape, data)
    }

    // './IphoneBody';
    // IphoneButtons
    // IphoneCamera
    // IphoneScreen

    render() {
        let shapeData = [
            'TableBody',
            'TableButtons',
        ]

        // LaptopCPU
        // LaptopFeet
        // LaptopKeys
        // LaptopMonitor
        // LaptopScreen
        // LaptopTouchPad
        return (
            <div className="three">
                <div className="particle">
                    {
                        shapeData.map((n, i) => {
                            return (
                                <div key={i}>
                                    <span>{n}:</span>

                                    <span>Px</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Px', n)} />
                                    <span>Py</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Py', n)} />
                                    <span>Pz</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Pz', n)} />

                                    <span>Rx</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Rx', n)} />
                                    <span>Ry</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Ry', n)} />
                                    <span>Rz</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Rz', n)} />

                                    <span>Scale</span><input type="text" onChange={(e, type, shape) => this.handleParticle(e, 'Scale', n)} />

                                </div>
                            )
                        })
                    }
                </div>
            </div >
        );
    }
}

export default ThreeIndex;
