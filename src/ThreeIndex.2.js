import React, { Component } from 'react';
import Mousetrap from 'mousetrap';

import ParticleShape from './ParticleShape'
import Particle from './Particle'
import PanelHandle from './PanelHandle'
import CouplePanelHandle from './CouplePanelHandle'

let THREE = window.THREE
class ThreeIndex extends Component {
    constructor(props, context) {
        super(props, context);
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 1000);
        this.renderer = new THREE.WebGLRenderer({ antialias: false });
        this.animate();
        this.textureIndex = 0
        this.textureIndexLeft = 0
        this.textureIndexRight = 0

        this.data = {}

        this.container;
        this.video
        this.texture
        this.material
        this.mesh;
        this.composer;
        this.cube_count
        this.meshes = []
        this.materials = []
        this.xgrid = 10
        this.ygrid = 10;
        this.h = 1;
        this.counter = 1;
        this.initThree();
    }

    initThree() {
        this.renderer.setSize(window.innerWidth, 2 * window.innerHeight / 3);
        window.scene = this.scene
        this.camera.position.z = 5;
        this.container = this.renderer.domElement//document.createElement('div');
        document.body.appendChild(this.container);

        this.camera.position.z = 1000;

        this.axes = new THREE.AxisHelper(100)
        this.scene.add(this.axes)

        this.light = new THREE.DirectionalLight(0xffffff);
        this.light.position.set(0.5, 1, 1).normalize();
        this.scene.add(this.light);

        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);

        
        
        
        this.video = document.getElementById('video');

        this.texture = new THREE.VideoTexture(this.video);
        this.texture.minFilter = THREE.LinearFilter;
        this.texture.magFilter = THREE.LinearFilter;
        this.texture.format = THREE.RGBFormat;
        let i, j, ux, uy, ox, oy,
            geometry,
            xsize, ysize;

        ux = 1 / this.xgrid;
        uy = 1 / this.ygrid;

        // xsize = 480 / xgrid;
        xsize = 408 / this.xgrid;
        ysize = 204 / this.ygrid;

        let parameters = { color: 0xffffff, map: this.texture };

        this.cube_count = 0;

        this.meshGroup = new THREE.Group();
        for (i = 0; i < this.xgrid; i++) {
            for (j = 0; j < this.ygrid; j++) {
                ox = i;
                oy = j;
                geometry = new THREE.BoxGeometry(xsize, ysize, xsize);
                this.change_uvs(geometry, ux, uy, ox, oy);
                this.materials[this.cube_count] = new THREE.MeshLambertMaterial(parameters);
                this.material = this.materials[this.cube_count];
                this.material.hue = i / this.xgrid;
                this.material.saturation = 1 - j / this.ygrid;
                let mesh = new THREE.Mesh(geometry, this.material);
                mesh.position.x = (i - this.xgrid / 2) * xsize;
                mesh.position.y = (j - this.ygrid / 2) * ysize;
                mesh.position.z = 0;
                mesh.scale.x = mesh.scale.y = mesh.scale.z = 1;
                mesh.name = 'mesh'
                // this.scene.add(mesh)
                mesh.dx = 0.001 * (0.5 - Math.random());
                mesh.dy = 0.001 * (0.5 - Math.random());
                this.meshes[this.cube_count] = mesh;
                this.cube_count += 1;
                this.meshGroup.add(mesh)
            }
        }
        this.scene.add(this.meshGroup);

        this.renderer.autoClear = false;
        let renderModel = new THREE.RenderPass(this.scene, this.camera);
        let effectBloom = new THREE.BloomPass(1.3);
        let effectCopy = new THREE.ShaderPass(THREE.CopyShader);
        effectCopy.renderToScreen = true;
        this.composer = new THREE.EffectComposer(this.renderer);
        this.composer.addPass(renderModel);
        this.composer.addPass(effectBloom);
        this.composer.addPass(effectCopy);
        window.addEventListener('resize', this.onWindowResize, false);
    }

    onWindowResize() {
        if (!this.camera) {
            this.camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 10000);
        }
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();

        if (!this.renderer) {
            this.renderer = new THREE.WebGLRenderer();
        }
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        if (!this.composer) {
            this.composer = new THREE.EffectComposer(this.renderer);
        }
        this.composer.reset();
    }

    change_uvs(geometry, unitx, unity, offsetx, offsety) {
        let faceVertexUvs = geometry.faceVertexUvs[0];
        for (let i = 0; i < faceVertexUvs.length; i++) {
            let uvs = faceVertexUvs[i];
            for (let j = 0; j < uvs.length; j++) {
                let uv = uvs[j];
                uv.x = (uv.x + offsetx) * unitx;
                uv.y = (uv.y + offsety) * unity;
            }
        }
    }


    animate() {
        if (this.meshes) {
            let time = Date.now() * 0.00005;
            this.camera.lookAt(this.scene.position);
            for (let i = 0; i < this.cube_count; i++) {
                this.material = this.materials[i];
                this.h = (360 * (this.material.hue + time) % 360) / 360;
                // this.material.color.setHSL(this.h, this.material.saturation, 0.5);
            }

            this.meshes.forEach(n => {
                // n.rotation.x += 0.01
                // n.rotation.y += 0.01
            })


            this.counter++;
            this.renderer.clear();
            this.composer.render();

            this.meshGroup.rotation.y += 0.01



            if ( this.counter % 1000 > 200 ) {
				for ( var i = 0; i < this.cube_count; i ++ ) {
					this.mesh = this.meshes[ i ];
					this.mesh.rotation.x += 10 * this.mesh.dx;
					this.mesh.rotation.y += 10 * this.mesh.dy;
					this.mesh.position.x += 200 * this.mesh.dx;
					this.mesh.position.y += 200 * this.mesh.dy;
					this.mesh.position.z += 400 * this.mesh.dx;
				}
			}

			if ( this.counter % 1000 === 0 ) {
				for ( var i = 0; i < this.cube_count; i ++ ) {
					this.mesh = this.meshes[ i ];
					this.mesh.dx *= -1;
					this.mesh.dy *= -1;
				}
			}


        }

        requestAnimationFrame(this.animate.bind(this));
        this.renderer.render(this.scene, this.camera);
    };

    changeMesh(data) {
        this.meshGroup.children[this.data.index].position.x += this.data.x * 1
        this.meshGroup.children[this.data.index].position.y += this.data.y * 1
        this.meshGroup.children[this.data.index].position.z += this.data.z * 1
        // this.meshGroup.children[this.data.index].rotation.x += this.data.a * 1
        // this.meshGroup.children[this.data.index].rotation.y += this.data.b * 1
        // this.meshGroup.children[this.data.index].rotation.z += this.data.c * 1
    }
    changeData(a, b) {
        this.data[a] = b.target.value
    }

    render() {
        return (
            <div>
                <div>
                    <span>Index</span><input type="text" onChange={(e) => this.changeData('index', e)} />
                    <span>x</span><input type="text" onChange={(e) => this.changeData('x', e)} />
                    <span>y</span><input type="text" onChange={(e) => this.changeData('y', e)} />
                    <span>z</span><input type="text" onChange={(e) => this.changeData('z', e)} />
                    <button onClick={() => this.changeMesh()}>Confirm</button>
                </div>
            </div>
        )
    }
}

export default ThreeIndex;
