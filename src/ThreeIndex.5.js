import React, { Component } from 'react';
import TextShape from './TextShape';

let THREE = window.THREE
class ThreeIndex extends Component {
    constructor(props, context) {
        super(props, context);
        THREE.Cache.enabled = true;
        this.container
        this.camera
        this.cameraTarget
        this.scene
        this.renderer;
        this.group
        this.textMesh1
        this.textGeo
        this.materials;
        this.text = "three.js"
        this.height = 20
        this.size = 70
        this.hover = 30
        this.curveSegments = 4
        this.bevelThickness = 2
        this.bevelSize = 1.5
        this.bevelSegments = 3
        this.bevelEnabled = true
        this.font = undefined
        this.fontName = "optimer" // helvetiker, optimer, gentilis, droid sans, droid serif
        this.fontWeight = "bold"; // normal bold
        this.TextShape = new TextShape()
        this.init();
        this.animate();
    }

    init() {
        this.container = document.createElement('div');
        document.body.appendChild(this.container);
        this.camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 1500);
        this.camera.position.set(0, 0, 700);
        this.cameraTarget = new THREE.Vector3(0, 0, 0);
        this.scene = new THREE.Scene();
        this.dirLight = new THREE.DirectionalLight(0xffffff, 0.125);
        this.dirLight.position.set(0, 0, 1).normalize();
        this.scene.add(this.dirLight);
        this.pointLight = new THREE.PointLight(0xffffff, 1.5);
        this.pointLight.position.set(0, 100, 90);
        this.scene.add(this.pointLight);
        this.pointLight.color.setHex(0xffffff);
        this.materials = [
            new THREE.MeshPhongMaterial({ color: 0xffffff, flatShading: true }), // front
            new THREE.MeshPhongMaterial({ color: 0xffffff }) // side
        ];

        // this.group = new THREE.Group();
        // this.group.position.y = 100;

        // console.log(new TextSxhape)
        console.log(this.TextShape.getMesh())
        // this.group = new TextShape.getMesh()
        // this.scene.add(this.group);

        // this.loadFont();

        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.container.appendChild(this.renderer.domElement);
    }
    animate() {
        requestAnimationFrame(this.animate.bind(this));
        this.render();
    }

    render() {
        this.camera.lookAt(this.cameraTarget);
        this.renderer.clear();
        this.renderer.render(this.scene, this.camera);
        return (
            <div>test</div>
        )
    }
}

export default ThreeIndex;
