import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import ThreeIndex from './ThreeIndex'
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      showNav: true
    }
  }
  change() {
    console.log(123)
    this.setState({
      showNav: false
    })
  }
  render() {
    console.log(this.state.showNav)
    return (
      <Router>
        <div>
          {
            this.state.showNav ? <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/topics">Topics</Link>
              </li>
            </ul> : null
          }
          <hr />
          {/* <Route exact path="/" component={Home} />
        <Route path="/about" component={About} /> */}
          <Route onClick={()=>this.change()} path="/topics" component={ThreeIndex} />
        </div>
      </Router>

      // <div className="App">
      // <ThreeIndex />
      // </div>
    );
  }
}

export default App;
