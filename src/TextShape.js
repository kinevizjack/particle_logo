import React, { Component } from 'react';
import * as THREE from 'three';


class TextShape extends Component {
    constructor() {
        super()

        this.text = "three.js"
        this.height = 20
        this.size = 70
        this.hover = 30
        this.curveSegments = 4
        this.bevelThickness = 2
        this.bevelSize = 1.5
        this.bevelSegments = 3
        this.bevelEnabled = true
        this.font = undefined
        this.fontName = "optimer" // helvetiker, optimer, gentilis, droid sans, droid serif
        this.fontWeight = "bold"; // normal bold

        this.group = new THREE.Group();
        this.init()
    }
    init() {
        // this.loadFont()
        this.loader = new THREE.FontLoader();
        this.loader.load('fonts/optimer_bold.typeface.json', (response) => {
            this.font = response;

            console.log(response)

            this.refreshText();
        });
    }

    getMesh() {
        return this.group
    }

    loadFont() {

        this.loader = new THREE.FontLoader();
        this.loader.load('fonts/' + this.fontName + '_' + this.fontWeight + '.typeface.json', (response) => {
            this.font = response;
            this.refreshText();
        });
    }
    createText() {
        this.materials = [
            new THREE.MeshPhongMaterial({ color: 0xff0000, flatShading: true }), // front
            new THREE.MeshPhongMaterial({ color: 0xff0000 }) // side
        ];
        this.textGeo = new THREE.TextBufferGeometry(this.text, {
            font: this.font,
            size: this.size,
            height: this.height,
            curveSegments: this.curveSegments,
            bevelThickness: this.bevelThickness,
            bevelSize: this.bevelSize,
            bevelEnabled: this.bevelEnabled,
            material: 0,
            extrudeMaterial: 1

        });
        this.textGeo.computeBoundingBox();
        this.textGeo.computeVertexNormals();
        this.centerOffset = -0.5 * (this.textGeo.boundingBox.max.x - this.textGeo.boundingBox.min.x);
        this.textMesh1 = new THREE.Mesh(this.textGeo, this.materials);
        this.textMesh1.position.x = 0//this.centerOffset;
        this.textMesh1.position.y = 0//this.hover;
        this.textMesh1.position.z = 0;
        this.textMesh1.rotation.x = 0;
        this.group.add(this.textMesh1);
    }

    refreshText() {
        this.group.remove(this.textMesh1);
        if (!this.text) return;
        this.createText();
    }

    update() {

    }
}

export default TextShape