import React, { Component } from 'react';
import * as THREE from 'three';


class PanelHandle extends Component {
    constructor(scene) {
        super()
        this.scene = scene;
        this.textureIndex = 1;
        this.initPanel();
    }

    handlePanel(textureIndex) {
        let plane = this.scene.getObjectByName('plane')
        if (textureIndex > 7) {
            plane.visible = false;
        } else {
            let url = 'checklist' + textureIndex + '.png';
            new THREE.TextureLoader().load(
                url,
                texture => {
                    texture.minFilter = THREE.LinearFilter;
                    plane.material.map = texture
                }
            )
        }
    }
    initPanel() {
        new THREE.TextureLoader().load(
            './checklist1.png',
            texture => {
                texture.minFilter = THREE.LinearFilter;
                let planeGeometry = new THREE.PlaneGeometry(1.5, 2, );
                let planeMaterial = new THREE.MeshBasicMaterial({ map: texture });
                let plane = new THREE.Mesh(planeGeometry, planeMaterial);
                plane.position.x = 0;
                plane.position.y = 0;
                plane.position.z = 0;
                plane.name = 'plane';
                // this.scene.add(plane);
            })
    }
}

export default PanelHandle;
