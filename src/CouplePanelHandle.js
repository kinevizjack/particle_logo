import React, { Component } from 'react';
import * as THREE from 'three';


class CouplePanelHandle extends Component {
    constructor(scene) {
        super()
        this.scene = scene;
        this.textureIndex = 1;
        this.initPanel();
    }

    handlePanel(leftIndex,rightIndex) {
        let planeLeft = this.scene.getObjectByName('plane-left')
        let planeRight = this.scene.getObjectByName('plane-right')
        if (leftIndex > 7) {
            planeLeft.visible = false;
        } else {
            let url = 'checklist' + leftIndex + '.png';
            new THREE.TextureLoader().load(
                url,
                texture => {
                    texture.minFilter = THREE.LinearFilter;
                    planeLeft.material.map = texture
                }
            )
        }
        if (rightIndex > 7) {
            planeRight.visible = false;
        } else {
            let url = 'checklist' + rightIndex + '.png';
            new THREE.TextureLoader().load(
                url,
                texture => {
                    texture.minFilter = THREE.LinearFilter;
                    planeRight.material.map = texture
                }
            )
        }
    }
    initPanel() {
        new THREE.TextureLoader().load(
            './checklist1.png',
            texture => {
                texture.minFilter = THREE.LinearFilter;
                let planeGeometry = new THREE.PlaneGeometry(1.5, 2);
                let planeMaterial = new THREE.MeshBasicMaterial({ map: texture });
                let planeLeft = new THREE.Mesh(planeGeometry, planeMaterial);
                planeLeft.name = 'plane-left';
                planeLeft.position.x = -2;
                planeLeft.position.y = 0;
                planeLeft.position.z = 0;
                // this.scene.add(planeLeft);

                let planeRightGeometry = new THREE.PlaneGeometry(1.5, 2);
                let planeRightMaterial = new THREE.MeshBasicMaterial({ map: texture });
                let planeRight = new THREE.Mesh(planeRightGeometry, planeRightMaterial);
                planeRight.name = 'plane-right';
                planeRight.position.x = 2;
                planeRight.position.y = 0;
                planeRight.position.z = 0;
                // this.scene.add(planeRight);
            })
    }
}

export default CouplePanelHandle;
