import React, { Component } from 'react';
import * as THREE from 'three';
import Mousetrap from 'mousetrap';
import TWEEN from 'tween.js'


class Particle extends Component {
    constructor(scene) {
        super()
        this.scene = scene;
        this.initParticle()
        this.handleMark()
        Mousetrap.bind('3', () => {
            this.markTweenIn.start();
        })
    }

    initParticle() {
        let planeGeometry = new THREE.PlaneGeometry(2, 4);
        let planeMaterial = new THREE.MeshBasicMaterial({ color: 0x00ffff });
        let plane = new THREE.Mesh(planeGeometry, planeMaterial);
        plane.position.x = 0;
        plane.position.y = 0;
        plane.position.z = 0;
        plane.name = 'plane';
        this.scene.add(plane);

        let cubeGeometry = new THREE.CubeGeometry(0.5, 0.5, 0.5);
        let cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        let cube = new THREE.Mesh(cubeGeometry, cubeMaterial)
        cube.position.x = 1.5
        cube.position.y = -2.5
        cube.position.z = 1.5
        cube.name = 'mark'
        this.scene.add(cube);

        new THREE.TextureLoader().load(
            './yes.png',
            texture => {
                let planeGeometry = new THREE.PlaneGeometry(0.5, 0.5);
                let planeMaterial = new THREE.MeshBasicMaterial({ map: texture });
                let plane = new THREE.Mesh(planeGeometry, planeMaterial);
                plane.position.x = 0.5
                plane.position.y = -1.6
                plane.visible = false;
                plane.name = 'watermark';
                plane.material.transparent = true;
                plane.material.needsUpdate = true;
                plane.material.opacity = 0.1;
                this.scene.add(plane);
            })
        let axi = new THREE.AxisHelper(20)
        this.scene.add(axi);
    }

    handleMark() {
        let markCube = this.scene.getObjectByName('mark')
        let positionOut = {
            x: 0.5,
            y: -1.5,
            z: 0.5,
            opacity: 1
        }
        this.markTweenOut = new TWEEN.Tween(positionOut)
            .to({
                x: -1,
                y: -2,
                z: 0.5,
                opacity: 0
            }, 2000)
            .easing(TWEEN.Easing.Quadratic.In)
            .onUpdate(() => {
                markCube.position.x = positionOut.x;
                markCube.position.y = positionOut.y;
                markCube.position.z = positionOut.z;
                markCube.material.opacity = positionOut.opacity;
                markCube.material.transparent = true;
                markCube.material.needsUpdate = true;
            })
            .onComplete(() => {
            })

        let positionIn = {
            x: 1.5,
            y: -2.5,
            z: 1.5,
        }
        this.markTweenIn = new TWEEN.Tween(positionIn)
            .to({
                x: 0.5,
                y: -1.5,
                z: 0.5
                // rotation: -1 * Math.PI
            }, 2000)
            .easing(TWEEN.Easing.Quadratic.In)
            .onUpdate(() => {
                markCube.position.x = positionIn.x;
                markCube.position.y = positionIn.y;
                markCube.position.z = positionIn.z;
                // markCube.verticesNeedUpdate = true;
            })
            .onComplete(() => {
                let markPanel = this.scene.getObjectByName('watermark')
                markPanel.visible = true
            })
            .chain(this.markTweenOut)
    }

    update() {
        TWEEN.update()
    }
}
export default Particle